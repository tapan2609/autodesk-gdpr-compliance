CREATE TABLE excel_data_task (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  number varchar(45) NOT NULL,
  request_id varchar(20) NOT NULL,
  status varchar(45) NOT NULL,
  service varchar(45) NOT NULL,
  reqtype varchar(45) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY unique_number (number)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

ALTER TABLE lambda_response
ADD COLUMN resason longtext NOT NULL AFTER updated_date,
ADD COLUMN forge_update_status BIT NOT NULL DEFAULT FALSE AFTER resason;