ALTER TABLE `excel_data_task`
ADD COLUMN `email_id` VARCHAR(100) NULL AFTER `fetch_status`;

ALTER TABLE `excel_data_task`
ADD COLUMN `client_id` VARCHAR(100) NULL AFTER `email_id`;
