CREATE TABLE vendor_details (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  vendor_name varchar(100) NOT NULL,
  client_id varchar(100) NOT NULL,
  client_secret varchar(100) NOT NULL,
  associate_lambda varchar(100) NOT NULL,
  last_execution_time datetime NULL,
  last_update datetime NOT NULL,
  last_updated_by varchar(255) NULL,
  PRIMARY KEY (id),
  UNIQUE KEY unique_client (client_id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE forge_transaction_log (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  vendor_id bigint(20) NOT NULL,
  auth_request_body longblob NULL,
  auth_response_body longblob NULL,
  get_request_body longblob NULL,
  get_response_body longblob NULL,
  auth_request_time datetime NULL,
  auth_response_time datetime NULL,
  get_request_time datetime NULL,
  get_response_time datetime NULL,
  auth_status BIT NOT NULL,
  get_status BIT NOT NULL,
  PRIMARY KEY (id),
  KEY forge_transaction_log_vendor_details_fk (vendor_id),
  CONSTRAINT forge_transaction_log_vendor_details_fk FOREIGN KEY (vendor_id) REFERENCES vendor_details (id) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE lambda_transaction_log (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  vendor_id bigint(20) NOT NULL,
  task longblob NOT NULL,
  lambda_request_body longblob NOT NULL,
  lambda_response_body longblob NOT NULL,
  lambda_request_time datetime NOT NULL,
  lambda_response_time datetime NOT NULL,
  forge_update_request_body longblob NOT NULL,
  forge_update_response_body longblob NOT NULL,
  forge_update_request_time datetime NOT NULL,
  forge_update_response_time datetime NOT NULL,
  initiated_type tinyint NOT NULL,
  lambda_status BIT NOT NULL,
  PRIMARY KEY (id),
  KEY lambda_transaction_log_vendor_details_fk (vendor_id),
  CONSTRAINT lambda_transaction_log_vendor_details_fk FOREIGN KEY (vendor_id) REFERENCES vendor_details (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE lambda_response (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  task_id varchar(100) NOT NULL,
  client_id varchar(100) NOT NULL,
  request_id varchar(100) NOT NULL,
  instance_id varchar(100) NOT NULL,
  user_id varchar(100) NOT NULL,
  status BIT NOT NULL,
  number_of_attempt bigint(20) NOT NULL,
  created_date datetime NOT NULL,
  updated_date datetime NOT NULL,
  PRIMARY KEY (id)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;