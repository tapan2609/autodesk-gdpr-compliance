INSERT INTO vendor_details
( id, vendor_name, client_id, client_secret, associate_lambda, last_execution_time, last_update, last_updated_by)
VALUES
('1', "Qualtrics", "FAH84KgJNzEldLuFwJEiCF8k3oXlTOQE", "uJ93NP4Cttc0ozm7", "gdpr_qualtrics_delete", now(), now(), "Deepak"),
('2', "SendGrid", "lUcwOmCbpeSkDfdCQzu3lI7SjPaPczyH", "RaOvoycFaRzzMyCk", "gdpr_sendgrid_delete", now(), now(), "Akash"),
('3', "Centercode", "Gm5EHu7VOOeaMmxpuzi0YUiaezdofCuQ", "Gq0TYPoTZP0Y5iGz", "", now(), now(), "Akash"),
('4', "Evergage", "0EdtbClPUcPRRjMcdYVAogGNKGjYZDGg", "6ecK2SgKXdFY05ZS", "gdpr_Evergage_delete", now(), now(), "Akash"),
('5', "Adobe Analytics (SaaS)", "3jnb7TGEDos8srGMOcBTBYUQAHFjKlIK", "L3hvq8mdx6MzLWqW", "gdpr_AdobeAnalytics_delete", now(), now(), "Akash"),
('6', "Google", "AxseEppJuWjMTMf2Aukrs2Gr9Imtabnv", "kaXaM6RrbKdas7oZ", "gdpr_google_delete", now(), now(), "Akash"),
('7', "MixPanel", "idtdjvnJngJDlBUblzGELfTkAW9793T4", "9N6do9jtLuv5JLAe", "", now(), now(), "Akash"),
('8', "DZone, Inc.", "MBNSLPJKPHAlHLJktWLR1Qm84MoUAV9j", "3N4Ha7NvWGEHJW9L", "gdpr_dZone_delete", now(), now(), "Akash");