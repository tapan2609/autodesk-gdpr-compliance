package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class UserInfo {

    private String id;
    private String email;
    private String name;
    private String user_type;

}
