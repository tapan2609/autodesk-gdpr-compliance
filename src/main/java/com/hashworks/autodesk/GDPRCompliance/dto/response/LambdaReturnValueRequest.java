package com.hashworks.autodesk.GDPRCompliance.dto.response;

import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaReturnStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LambdaReturnValueRequest {
    String taskId;
    LambdaReturnStatus status;
}



