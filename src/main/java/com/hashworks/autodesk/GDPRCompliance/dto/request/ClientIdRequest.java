package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
public class ClientIdRequest {

    @NotNull
    @Min(value = 1, message = "clientId should be greater than Zero!" )
    private String clientId;
//    private String taskId;
}
