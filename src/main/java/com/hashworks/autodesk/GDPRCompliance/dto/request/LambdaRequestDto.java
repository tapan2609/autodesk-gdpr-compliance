package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LambdaRequestDto {

    private String requestId;
    private String instanceId;
    private String functionName;
}
