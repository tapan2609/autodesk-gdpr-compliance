package com.hashworks.autodesk.GDPRCompliance.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ForgeResponse {

    private String status;
    private String description;
    private String number;
    private String userType;
    private String requestType;
    private String requestNumber;
    private String clientId;
    private String createdDate;
    private String notifyDate;
    private String userO2Id;
    private String userName;
    private String userEmail;
    private String appName;
}
