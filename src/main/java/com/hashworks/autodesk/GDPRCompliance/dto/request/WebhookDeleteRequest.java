package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class WebhookDeleteRequest {

    private String version;
    private HookRequest hook;
    private PayloadRequest payload;




}
