package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PayloadRequest {


    private String version;
    private String taskId;
    private UserInfo user_info;
    private String callbackUrl;
    private String respondBy;
    private String status;


}
