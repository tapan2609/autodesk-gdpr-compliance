package com.hashworks.autodesk.GDPRCompliance.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class QualtricsListResponse {
    private List<QualtricsResponse> task;
}
