package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
public class HookRequest {

    private String eventType;
    private String clientID;
    private String hookId;
    private String webhookEndPoint;
    private String createdDate;
    // private Date createdDate;

}
