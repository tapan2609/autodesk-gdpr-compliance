package com.hashworks.autodesk.GDPRCompliance.dto.request;

import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaReturnStatus;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class TaskIdListRequest {

    String taskId;
    LambdaReturnStatus lambdaReturnStatus;
}
