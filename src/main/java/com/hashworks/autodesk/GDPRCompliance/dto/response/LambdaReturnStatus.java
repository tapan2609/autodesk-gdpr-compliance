package com.hashworks.autodesk.GDPRCompliance.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class LambdaReturnStatus {
    String code;
    String type;
    String reason;
}
