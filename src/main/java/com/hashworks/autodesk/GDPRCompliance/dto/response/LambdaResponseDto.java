package com.hashworks.autodesk.GDPRCompliance.dto.response;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LambdaResponseDto {

    private String requestId;
    private String user;
}
