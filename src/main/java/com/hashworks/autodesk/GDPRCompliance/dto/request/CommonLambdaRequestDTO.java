package com.hashworks.autodesk.GDPRCompliance.dto.request;

import lombok.Data;


@Data
public class CommonLambdaRequestDTO {

    private String emailId;

    private String clientId;

    private String taskId;

    private String functionName;

    private String requestId;

    private String userId;

    private String instanceId;

}
