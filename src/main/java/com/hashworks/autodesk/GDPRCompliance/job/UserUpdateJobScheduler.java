package com.hashworks.autodesk.GDPRCompliance.job;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hashworks.autodesk.GDPRCompliance.exception.EntityNotFoundException;
import com.hashworks.autodesk.GDPRCompliance.service.ForgeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import java.util.Date;

@Slf4j
@Component
public class UserUpdateJobScheduler {

    @Autowired
    private ForgeService forgeService;

//    @Scheduled(cron = "0 0 0/1 ? * *")   //every 1hr
    @Scheduled(cron = "0 0/10 * * * ?")   //every 10min
    public void scheduledUserUpdate() throws JsonProcessingException, EntityNotFoundException, InterruptedException {
//        this.forgeService.getScheduledUserUpdate();
//        log.info("scheduler running at "+new Date());
    }

    @Scheduled(cron = "0 0 0/1 ? * *")   //every 1hr
    public void scheduledUserUpdateToForge() throws JsonProcessingException, EntityNotFoundException, InterruptedException {
//        this.forgeService.updateToForgeRequest();
        log.info("scheduler running at "+new Date());
    }
}
