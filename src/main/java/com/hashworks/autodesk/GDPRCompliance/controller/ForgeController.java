package com.hashworks.autodesk.GDPRCompliance.controller;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hashworks.autodesk.GDPRCompliance.dto.request.ClientIdRequest;
import com.hashworks.autodesk.GDPRCompliance.exception.EntityNotFoundException;
import com.hashworks.autodesk.GDPRCompliance.service.ForgeService;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

@RestController
@RequestMapping("/api/vendor")
@Slf4j
public class ForgeController {

    @Autowired
    private ForgeService forgeService;

    @RequestMapping(value = "back-log-forge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getBackLogForge(@RequestBody ClientIdRequest clientIdRequest) throws JSONException, JsonProcessingException, EntityNotFoundException, InterruptedException {
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(this.forgeService.getBackLogForge(clientIdRequest)), HttpStatus.OK);
    }

//    @RequestMapping(value = "back-log-forge-taskId", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Object> getBackLogForgeFromTaskId(@RequestBody ClientIdRequest clientIdRequest)
//            throws JSONException, JsonProcessingException, EntityNotFoundException {
//        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(this.forgeService.getBackLogForgeFromTaskId(clientIdRequest)), HttpStatus.OK);
//    }

    @RequestMapping(value = "all-back-log-forge", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllBackLogForge(@RequestBody ClientIdRequest clientIdRequest)
            throws JSONException {
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "forge-update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getUpdateForge(@RequestParam("clientId") String clientId)
            throws JSONException, JsonProcessingException, EntityNotFoundException, InterruptedException {
        this.forgeService.getScheduledUserUpdate(clientId);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "task-to-forge-data-update", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> updateToForgeRequest1(@RequestParam("clientId") String clientId)
            throws JSONException {
        this.forgeService.updateToForgeRequest(clientId);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "delete-excel-sheet-data",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteExceldataTask(@RequestParam("clientId") String clientId){
        Map resposen=forgeService.deleteDataUsingExcel(clientId);
        return new ResponseEntity<>(resposen,HttpStatus.OK);
    }



    @RequestMapping(value = "back-log-task-forge-details", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> getAllBackLogTaskFromForge(@RequestBody ClientIdRequest clientIdRequest)
            throws JSONException, EntityNotFoundException, InterruptedException, JsonProcessingException {
        this.forgeService.getAllTasksFromForge(clientIdRequest);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }


    @RequestMapping(value = "delete-from-vendor",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> deleteDataFromVendor(@RequestBody ClientIdRequest clientIdRequest)   throws JSONException, EntityNotFoundException, InterruptedException, JsonProcessingException {
        this.forgeService.getAllTasksFromForgeAndProcessForDelete(clientIdRequest);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }

    @RequestMapping(value = "check-for update-status",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> checkUpdateStatusOfTask(@RequestBody ClientIdRequest clientIdRequest)   throws JSONException, EntityNotFoundException, InterruptedException, JsonProcessingException {
        this.forgeService.checkUpdateDetailsOfVendor(clientIdRequest);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }

}
