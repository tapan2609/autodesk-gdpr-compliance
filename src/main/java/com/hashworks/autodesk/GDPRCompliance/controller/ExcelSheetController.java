package com.hashworks.autodesk.GDPRCompliance.controller;

import com.hashworks.autodesk.GDPRCompliance.model.ExcelDataTask;
import com.hashworks.autodesk.GDPRCompliance.repository.ExcelDataTaskRepository;
import com.hashworks.autodesk.GDPRCompliance.service.ForgeService;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@Slf4j
@RequestMapping("/api/excel/")
@CrossOrigin
public class ExcelSheetController {

    @Autowired
    private ForgeService forgeService;

    @Autowired
    private ExcelDataTaskRepository excelDataTaskRepository;

    @RequestMapping(value = "download", method = RequestMethod.GET)
    public String downloadGet(@RequestParam("clientId") String clientId,
                              Model model, HttpServletRequest request,
                              HttpServletResponse response, HttpSession httpSession)  {
        model.addAttribute("forgeList", forgeService.getAllBackLogForge(clientId, request, response, httpSession));
        return "";
    }


    @RequestMapping(value = "excel-import", method = RequestMethod.POST)
    public ResponseEntity<?> mapReapExcelDatatoDB(@RequestPart MultipartFile reapExcelDataFile) throws IOException {
        List<ExcelDataTask> excelDataTaskArrayList = new ArrayList<ExcelDataTask>();
        XSSFWorkbook workbook = new XSSFWorkbook(reapExcelDataFile.getInputStream());
        XSSFSheet worksheet = workbook.getSheetAt(0);
        for(int i=1;i<worksheet.getPhysicalNumberOfRows() ;i++) {
            ExcelDataTask excelDataTask = new ExcelDataTask();
            XSSFRow row = worksheet.getRow(i);
          /*  excelDataTask.setNumber(row.getCell(0).getStringCellValue());
            excelDataTask.setService(row.getCell(1).getStringCellValue());
            excelDataTask.setStatus(row.getCell(3).getStringCellValue());
            excelDataTask.setReqtype(row.getCell(4).getStringCellValue());
            excelDataTask.setRequestId(row.getCell(6).getStringCellValue());
            excelDataTask.setFetchStatus(false);*/

            excelDataTask.setNumber(row.getCell(6).getStringCellValue());
            excelDataTask.setRequestId(row.getCell(0).getStringCellValue());
            excelDataTask.setStatus(row.getCell(9).getStringCellValue());
            excelDataTask.setService(row.getCell(8).getStringCellValue());
            excelDataTask.setReqtype(row.getCell(7).getStringCellValue());
            excelDataTask.setEmailId(row.getCell(2).getStringCellValue());
            excelDataTask.setClientId("jQw6o7Lzu/mTOhh7a2C7eVLgcQNDz/0vaNOomcF5Zr0=");//Lithium client_id

            excelDataTaskArrayList.add(excelDataTask);
        }
        this.excelDataTaskRepository.saveAll(excelDataTaskArrayList);
        log.info("saved the excel data to database"+excelDataTaskArrayList);
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(), HttpStatus.OK);
    }


}