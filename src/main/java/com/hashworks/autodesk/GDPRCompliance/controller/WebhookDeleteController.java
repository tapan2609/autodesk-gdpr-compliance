package com.hashworks.autodesk.GDPRCompliance.controller;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.hashworks.autodesk.GDPRCompliance.dto.request.WebhookDeleteRequest;
import com.hashworks.autodesk.GDPRCompliance.exception.EntityNotFoundException;
import com.hashworks.autodesk.GDPRCompliance.service.WebhookDeleteService;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/vendor/webhook")
@Slf4j
public class WebhookDeleteController {

    @Autowired
    WebhookDeleteService webhookDeleteService;

    @RequestMapping(value = "deleteEvent",method = RequestMethod.POST,produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> webhookDeleteEvent(@RequestBody WebhookDeleteRequest webhookDeleteRequest) throws JSONException, JsonProcessingException, EntityNotFoundException {
       log.info("<=== Started Executing Delete event ===>");
        return new ResponseEntity<>(ResponseJsonUtil.getSuccessResponseJson(this.webhookDeleteService.processDeleteEvent(webhookDeleteRequest)),HttpStatus.OK);
    }
}
