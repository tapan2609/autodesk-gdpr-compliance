package com.hashworks.autodesk.GDPRCompliance.model;

import lombok.*;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class VendorDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String vendorName;
    private String clientId;
    private String clientSecret;
    private String associateLambda;
    private Date lastExecutionTime;
    private Date lastUpdate;
    private String lastUpdatedBy;
}
