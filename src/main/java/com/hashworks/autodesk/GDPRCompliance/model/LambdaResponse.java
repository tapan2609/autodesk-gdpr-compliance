package com.hashworks.autodesk.GDPRCompliance.model;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.util.Date;

@Data
@NoArgsConstructor
@Entity
public class LambdaResponse {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String taskId;
    private String requestId;
    private String instanceId;
    private String clientId;
    private String userId;
    private Boolean status = false;
    private Long numberOfAttempt;
    private Date createdDate = new Date();
    private Date updatedDate = new Date();
    private String resason;
    private Boolean forgeUpdateStatus = false;

}
