package com.hashworks.autodesk.GDPRCompliance.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
public class ExcelDataTask {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String number;
    private String requestId;
    private String status;
    private String service;
    private String reqtype;
    private Boolean fetchStatus = false;
    private String emailId;
    private String clientId;
}
