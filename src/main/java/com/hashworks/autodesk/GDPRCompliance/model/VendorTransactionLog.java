package com.hashworks.autodesk.GDPRCompliance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.hashworks.autodesk.GDPRCompliance.model.enumeration.InitiatedType;
import lombok.Data;
import lombok.NoArgsConstructor;
import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class VendorTransactionLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String task;
    private String lambdaRequestBody;
    private String lambdaResponseBody;
    private Date lambdaRequestTime;
    private Date lambdaResponseTime;
    private Boolean lambdaStatus;
    private InitiatedType initiatedType;
    private String forgeUpdateRequestBody;
    private String forgeUpdateResponseBody;
    private Date forgeUpdateRequestTime;
    private Date forgeUpdateResponseTime;
    @ManyToOne
    @JoinColumn(name = "vendor_id")
    @JsonIgnore
    private  VendorDetails vendorDetails;
}
