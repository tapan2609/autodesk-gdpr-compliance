package com.hashworks.autodesk.GDPRCompliance.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
public class ForgeTransactionLog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    private String authRequestBody;
    private String authResponseBody;
    private String getRequestBody;
    private String getResponseBody;
    private Date authRequestTime;
    private Date authResponseTime;
    private Date getRequestTime;
    private Date getResponseTime;
    private Boolean authStatus;
    private Boolean getStatus;
    @ManyToOne
    @JoinColumn(name = "vendor_id")
    @JsonIgnore
    private  VendorDetails vendorDetails;
}
