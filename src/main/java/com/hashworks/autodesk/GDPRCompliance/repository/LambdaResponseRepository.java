package com.hashworks.autodesk.GDPRCompliance.repository;

import com.hashworks.autodesk.GDPRCompliance.model.LambdaResponse;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface LambdaResponseRepository extends JpaRepository<LambdaResponse, Long> {

    @Query(value = "SELECT * FROM lambda_response WHERE forge_update_status<>true AND client_id = ?1  LIMIT 3000",nativeQuery = true)
    List<LambdaResponse> getAllLambdaByTime(String clientId);
//    @Query(value = "SELECT * FROM lambda_response WHERE status<>true AND task_id IN (:taskId)",nativeQuery = true)
//    List<LambdaResponse> getAllLambdaByTime(@Param("taskId") List<String> taskId);

    @Query(value = "SELECT * FROM lambda_response WHERE task_id IN (:taskId)",nativeQuery = true)
    List<LambdaResponse> findAllByTaskId(@Param("taskId") List<String> taskId);

//    @Query(value = "update lambda_response set updated_date = now(), status = true, forge_update_status = false," +
//            "resason = :resason where task_id = ")
//    Boolean updateVendorResponse();

    List<LambdaResponse> findAllByTaskId(String taskId);

    @Query(value = "select distinct(task_id),resason,client_id from lambda_response where client_id = :clientId AND status <> false AND forge_update_status <> true LIMIT 3000", nativeQuery = true)
    List<Object[]> findAllByFetchStatus(@Param("clientId") String clientId);

    @Query(value = "select DISTINCT(taskId) from LambdaResponse WHERE forgeUpdateStatus<>true AND clientId = :clientId")
    List<String> findByClientId(@Param("clientId") String clientId);

    @Query(value = "SELECT * FROM lambda_response WHERE forge_update_status <> true AND status <> true AND client_id = ?1  LIMIT 3000",nativeQuery = true)
    List<LambdaResponse> findAllRequestIdByClientId(String clientId);
}
