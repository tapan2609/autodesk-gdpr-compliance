package com.hashworks.autodesk.GDPRCompliance.repository;

import com.hashworks.autodesk.GDPRCompliance.model.ExcelDataTask;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface ExcelDataTaskRepository extends JpaRepository<ExcelDataTask, Long> {

    @Query(value = "SELECT number FROM excel_data_task WHERE fetch_status<>true LIMIT 20",nativeQuery = true)
    List<String> findAllByTaskIdAndStatus();

    List<ExcelDataTask> findByClientIdAndFetchStatus(@Param("clientId") String clientId,@Param("fetchStatus") Boolean fetchStatus);

    ExcelDataTask findOneByNumber(String taskId);
}
