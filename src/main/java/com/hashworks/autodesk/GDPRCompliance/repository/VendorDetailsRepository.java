package com.hashworks.autodesk.GDPRCompliance.repository;

import com.hashworks.autodesk.GDPRCompliance.model.VendorDetails;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface VendorDetailsRepository extends JpaRepository<VendorDetails,Long>{

    VendorDetails findOneByClientId(String clientId);


}
