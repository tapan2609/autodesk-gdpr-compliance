package com.hashworks.autodesk.GDPRCompliance.viewResolver;

import com.hashworks.autodesk.GDPRCompliance.dto.response.ForgeResponse;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Map;

@Component
public class ExcelView extends AbstractXlsView {

    @Override
    protected void buildExcelDocument(Map<String, Object> model,
                                      Workbook workbook,
                                      HttpServletRequest request,
                                      HttpServletResponse response) {

        // change the file name
        response.setHeader("Content-disposition", "attachment; filename=ForgeTaskList.xls");

        @SuppressWarnings("unchecked")
        List<ForgeResponse> forgeResponseList = (List<ForgeResponse>) model.get("forgeList");

        // create excel xls sheet
        Sheet sheet = workbook.createSheet("Forge_Task_Info_Sheet");
        sheet.setDefaultColumnWidth(25);

        // create style for header cells
        CellStyle style = workbook.createCellStyle();
        Font font = workbook.createFont();
        font.setFontName("Arial");
        style.setFillForegroundColor(HSSFColor.BLUE.index);
        style.setFillPattern(FillPatternType.SOLID_FOREGROUND);
        font.setBold(true);
        font.setColor(HSSFColor.WHITE.index);
        style.setFont(font);

        // create header row
        Row header = sheet.createRow(0);
        header.createCell(0).setCellValue("Status");
        header.getCell(0).setCellStyle(style);
        header.createCell(1).setCellValue("Description");
        header.getCell(1).setCellStyle(style);
        header.createCell(2).setCellValue("Number");
        header.getCell(2).setCellStyle(style);
        header.createCell(3).setCellValue("UserType");
        header.getCell(3).setCellStyle(style);
        header.createCell(4).setCellValue("RequestType");
        header.getCell(4).setCellStyle(style);
        header.createCell(5).setCellValue("RequestNumber");
        header.getCell(5).setCellStyle(style);
        header.createCell(6).setCellValue("ClientId");
        header.getCell(6).setCellStyle(style);
        header.createCell(7).setCellValue("CreatedDate");
        header.getCell(7).setCellStyle(style);
        header.createCell(8).setCellValue("NotifyDate");
        header.getCell(8).setCellStyle(style);
        header.createCell(9).setCellValue("UserO2Id");
        header.getCell(9).setCellStyle(style);
        header.createCell(10).setCellValue("UserName");
        header.getCell(10).setCellStyle(style);
        header.createCell(11).setCellValue("UserEmail");
        header.getCell(11).setCellStyle(style);
        header.createCell(12).setCellValue("AppName");
        header.getCell(12).setCellStyle(style);

        int rowCount = 1;

        for(ForgeResponse forgeResponse : forgeResponseList){
            Row batchRow =  sheet.createRow(rowCount++);
            batchRow.createCell(0).setCellValue(forgeResponse.getStatus());
            batchRow.createCell(1).setCellValue(forgeResponse.getDescription());
            batchRow.createCell(2).setCellValue(forgeResponse.getNumber());
            batchRow.createCell(3).setCellValue(forgeResponse.getUserType());
            batchRow.createCell(4).setCellValue(forgeResponse.getRequestType());
            batchRow.createCell(5).setCellValue(forgeResponse.getRequestNumber());
            batchRow.createCell(6).setCellValue(forgeResponse.getClientId());
            batchRow.createCell(7).setCellValue(forgeResponse.getCreatedDate());
            batchRow.createCell(8).setCellValue(forgeResponse.getNotifyDate());
            batchRow.createCell(9).setCellValue(forgeResponse.getUserO2Id());
            batchRow.createCell(10).setCellValue(forgeResponse.getUserName());
            batchRow.createCell(11).setCellValue(forgeResponse.getUserEmail());
            batchRow.createCell(12).setCellValue(forgeResponse.getAppName());
        }
    }

}
