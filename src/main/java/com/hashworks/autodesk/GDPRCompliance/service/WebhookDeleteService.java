package com.hashworks.autodesk.GDPRCompliance.service;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.hashworks.autodesk.GDPRCompliance.dto.request.WebhookDeleteRequest;
import com.hashworks.autodesk.GDPRCompliance.exception.EntityNotFoundException;
import com.hashworks.autodesk.GDPRCompliance.model.LambdaResponse;
import com.hashworks.autodesk.GDPRCompliance.model.VendorDetails;
import com.hashworks.autodesk.GDPRCompliance.model.constant.Constant;
import com.hashworks.autodesk.GDPRCompliance.repository.LambdaResponseRepository;
import com.hashworks.autodesk.GDPRCompliance.repository.VendorDetailsRepository;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseConstants;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

@Service
@Slf4j
public class WebhookDeleteService {

    @Autowired
    VendorDetailsRepository vendorDetailsRepository;

    @Autowired
    LambdaResponseRepository lambdaResponseRepository;

    public boolean processDeleteEvent(WebhookDeleteRequest webhookDeleteRequest) throws EntityNotFoundException {

        log.info("Inside processDeleteEvent() method");

        if (webhookDeleteRequest == null) {
            return false;
        }
        String payload = "";

        String clientId = webhookDeleteRequest.getHook().getClientID();
        String emailId = webhookDeleteRequest.getPayload().getUser_info().getEmail();

        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientId);

        if (vendorDetails == null) {
            log.error("No vendorDetails found for the clientId" + clientId);
            throw new EntityNotFoundException(VendorDetails.class, "VendorDetails " + Constant.NOT_FOUND,
                    " clientId : " + clientId);
        }

        if(emailId == null){
            log.error("No target user found :" + emailId);
            return false;
        }
        InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(vendorDetails.getAssociateLambda()).withPayload(ByteBuffer.wrap(emailId.getBytes()));
        InvokeResult invokeResult = awsLambdaClient().invoke(invokeRequest);
        payload = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
        log.info("<========= payload data =======>" + payload);



        JSONObject jsonObject = new JSONObject(payload);
        JSONObject json = new JSONObject();

        if (jsonObject.has(ResponseConstants.RESULTS)) {
            JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray(ResponseConstants.RESULTS);
            List<LambdaResponse> lambdaResponseList = new ArrayList<>();
            for (int j = 0, size2 = jsonArray.length(); j < size2; j++) {
                json = jsonArray.getJSONObject(j);
                LambdaResponse lambdaResponse = new LambdaResponse();
                lambdaResponse.setTaskId(webhookDeleteRequest.getPayload().getTaskId());
                lambdaResponse.setClientId(clientId);
                lambdaResponse.setUserId(json.getString("user"));
                lambdaResponse.setRequestId(json.getString("requestId"));
                lambdaResponse.setInstanceId(json.getString("instanceId"));
                lambdaResponse.setStatus(false);
                lambdaResponse.setNumberOfAttempt(0l);
                log.info("<===== saving lambda response data to database :: ======>:: " + lambdaResponse);
                lambdaResponseList.add(lambdaResponse);
            }
            this.lambdaResponseRepository.saveAll(lambdaResponseList);
            log.info("<===== this is the response result for the restTemplate ======>:: " +
                    json.getString("requestId") + "getUser data:: " + json.getString("user"));
            return true;
        } else {
            log.info("===== this no results found =====" + jsonObject);
            return false;
        }

    }
    @Bean
    public AWSLambda awsLambdaClient(){
        InstanceProfileCredentialsProvider provider = new InstanceProfileCredentialsProvider(true);
        return AWSLambdaClientBuilder.standard().withCredentials(provider).withRegion("us-west-2").build();
    }

}
