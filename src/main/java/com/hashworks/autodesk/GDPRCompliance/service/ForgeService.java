package com.hashworks.autodesk.GDPRCompliance.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.hashworks.autodesk.GDPRCompliance.dto.request.*;
import com.hashworks.autodesk.GDPRCompliance.dto.response.ForgeResponse;
import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaReturnStatus;
import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaReturnValueRequest;
import com.hashworks.autodesk.GDPRCompliance.exception.DataFoundNullException;
import com.hashworks.autodesk.GDPRCompliance.exception.EntityNotFoundException;
import com.hashworks.autodesk.GDPRCompliance.model.ExcelDataTask;
import com.hashworks.autodesk.GDPRCompliance.model.LambdaResponse;
import com.hashworks.autodesk.GDPRCompliance.model.VendorDetails;
import com.hashworks.autodesk.GDPRCompliance.model.constant.Constant;
import com.hashworks.autodesk.GDPRCompliance.repository.ExcelDataTaskRepository;
import com.hashworks.autodesk.GDPRCompliance.repository.LambdaResponseRepository;
import com.hashworks.autodesk.GDPRCompliance.repository.VendorDetailsRepository;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseConstants;
import com.hashworks.autodesk.GDPRCompliance.utils.ResponseJsonUtil;
import lombok.extern.slf4j.Slf4j;
import org.apache.catalina.servlet4preview.http.HttpServletRequest;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.*;

@Service
@Slf4j
public class ForgeService {

    @Value("${app.auth.url}")
    private String url;

    @Value("${app.grant.type}")
    private String grantType;

    @Value("${app.forge.url}")
    private String furl;

    @Value("${app.update.url}")
    private String forgeUpdateUrl;

    @Autowired
    private VendorDetailsRepository vendorDetailsRepository;

    @Autowired
    private ExcelDataTaskRepository excelDataTaskRepository;

    @Autowired
    private LambdaResponseRepository lambdaResponseRepository;

    @Autowired
    private VendorService vendorService;


    public boolean getBackLogForge(ClientIdRequest clientIdRequest) throws JSONException, JsonProcessingException,
            EntityNotFoundException, InterruptedException {
        log.info("<======= Started getBackLogForge() service ========>");
        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientIdRequest.getClientId());
        if (vendorDetails == null) {
            log.error("No vendorDetails found for the clientId" + clientIdRequest.getClientId());
            throw new EntityNotFoundException(VendorDetails.class, "VendorDetails " + Constant.NOT_FOUND,
                    " clientId : " + clientIdRequest.getClientId());
        }
        ResponseEntity<String> response = this.getStringResponseEntity(vendorDetails);


        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject json = new JSONObject();
        jsonObject.getJSONArray(ResponseConstants.TASKS);
        JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray(ResponseConstants.TASKS);

        log.info("Forge get tasks ==>> " + jsonObject.toString());

        LambdaTestDto lambdaTestDto = new LambdaTestDto();
        List<String> tasksId = this.lambdaResponseRepository.findByClientId(clientIdRequest.getClientId());
        log.info("===== task id ===== :: " + tasksId);
        for (int i = 0, size = jsonArray.length(); i < size; i++) {
            JSONObject objectInArray = jsonArray.getJSONObject(i);
            if (tasksId.indexOf(objectInArray.getString("number")) == -1) {
                List<String> listOfEmailId = new ArrayList<>();
                listOfEmailId.add(objectInArray.getString("user_email"));
                lambdaTestDto.setEmails(listOfEmailId);
                lambdaTestDto.setFunctionName(vendorDetails.getAssociateLambda());
                String payload = this.vendorService.qualtricsLambdaCall(lambdaTestDto);
                Thread.sleep(750);
                JSONObject jsonObject1 = new JSONObject(payload);
                if (jsonObject1.has(ResponseConstants.RESULTS)) {
                    JSONArray jsonArray1 = (JSONArray) jsonObject1.getJSONArray(ResponseConstants.RESULTS);
                    String taskId = (objectInArray.getString(ResponseConstants.NUMBER));
                    List<LambdaResponse> lambdaResponseList = new ArrayList<>();
                    for (int j = 0, size2 = jsonArray1.length(); j < size2; j++) {
                        json = jsonArray1.getJSONObject(j);
                        LambdaResponse lambdaResponse = new LambdaResponse();
                        lambdaResponse.setTaskId(taskId);
                        lambdaResponse.setClientId(clientIdRequest.getClientId());
                        lambdaResponse.setUserId(json.getString("user"));
                        lambdaResponse.setRequestId(json.getString("requestId"));
                        lambdaResponse.setInstanceId(json.getString("instanceId"));
                        lambdaResponse.setStatus(false);
                        lambdaResponse.setNumberOfAttempt(0l);
                        lambdaResponse.setResason("");
                        log.info("<===== saving lambda data to database :: ======>:: " + lambdaResponse);
                        lambdaResponseList.add(lambdaResponse);
                    }
                    this.lambdaResponseRepository.saveAll(lambdaResponseList);
                    log.info("<===== this is the response result for the restTemplate ======>:: " +
                            json.getString("requestId") + "getUser data:: " + json.getString("user"));
                } else {
                    log.info("===== this no results found =====" + jsonObject1);
                }
            }
        }
        return true;
    }

    public JSONArray convertToJsonArray(ResponseEntity<String> response) {

        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject json = new JSONObject();
        jsonObject.getJSONArray(ResponseConstants.TASKS);
        return (JSONArray) jsonObject.getJSONArray(ResponseConstants.TASKS);

    }

//    public boolean getBackLogForgeFromTaskId(ClientIdRequest clientIdRequest) throws JSONException, JsonProcessingException,
//            EntityNotFoundException {
//        log.info("<======= Started getBackLogForge() service ========>");
//        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientIdRequest.getClientId());
//        if (vendorDetails == null){
//            log.error("No vendorDetails found for the clientId "+clientIdRequest.getClientId());
//            throw new EntityNotFoundException(VendorDetails.class, "VendorDetails "+Constant.NOT_FOUND,
//                    " clientId : "+clientIdRequest.getClientId());
//        }
//        ResponseEntity<String> response = this.getStringResponseEntity(vendorDetails);
//        JSONObject jsonObject = new JSONObject(response.getBody());
//        jsonObject.getJSONArray(ResponseConstants.TASKS);
//        JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray(ResponseConstants.TASKS);
//        //int i = 0, size = jsonArray.length(); i < size; i++
//        LambdaTestDto lambdaTestDto = new LambdaTestDto();
//        for (int i = 0, size = 0; i < size; i++){
//            List<String> listOfEmailId = new ArrayList<>();
//            JSONObject objectInArray = jsonArray.getJSONObject(i);
//            if ((objectInArray.getString(ResponseConstants.NUMBER)).equalsIgnoreCase(clientIdRequest.getTaskId())){
//                listOfEmailId.add(objectInArray.getString(ResponseConstants.USEREMAIL));
//                lambdaTestDto.setEmails(listOfEmailId);
//                lambdaTestDto.setFunctionName(vendorDetails.getAssociateLambda());
//                String taskId = (objectInArray.getString(ResponseConstants.NUMBER));
////                LambdaResponseDto lambdaResponseDto = this.vendorService.qualtricsLambdaCall(lambdaTestDto);
////                log.info("<===== this is the response for the restTemplate ======>:: "+ lambdaResponseDto);
////                Boolean str = this.updateForge(clientIdRequest.getClientId(), taskId);
////                log.info("===forge update ===" +str);
//                break;
//            }
//        }
//        return true;
//    }

    private ResponseEntity<String> getStringResponseEntity(VendorDetails vendorDetails) throws JSONException {
        log.info("<====== started getStringResponseEntity(VendorDetails vendorDetails) =======> ");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add(ResponseConstants.CLIENT_ID, vendorDetails.getClientId());
        headers.add(ResponseConstants.CLIENT_SECRET, vendorDetails.getClientSecret());
        headers.add(ResponseConstants.GRANT_TYPE, grantType);
        ResponseEntity<String> res = restTemplate.postForEntity(url, headers, String.class);
        JSONObject jsonObject = new JSONObject(res.getBody());
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(ResponseConstants.AUTHORIZATION, jsonObject.getString(ResponseConstants.TOKEN_TYPE) + " " + jsonObject.getString(ResponseConstants.ACCESS_TOKEN));
        HttpEntity<String> entity = new HttpEntity<>(ResponseConstants.PARAMETERS, headers);
        log.info("making rest template call the forge with entity data" + entity);
        return restTemplate.exchange(furl, HttpMethod.GET, entity, String.class);
    }

    public List<ForgeResponse> getAllBackLogForge(String clientId, HttpServletRequest request,
                                                  HttpServletResponse response,
                                                  HttpSession httpSession) {

        Map<String, Object> model = new HashMap<>();
        model.put("sheetname", "Forge_Task_Info_Sheet");

        log.info("<======= Started getAllBackLogForge() service ========>");
        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientId);
        ResponseEntity<String> responseEntity = this.getStringResponseEntity(vendorDetails);
        JSONObject jsonObject = new JSONObject(responseEntity.getBody());
        JSONArray jsonArray = (JSONArray) jsonObject.getJSONArray(ResponseConstants.TASKS);
        List<ForgeResponse> forgeResponses = new ArrayList<>();
        for (int i = 0, size = jsonArray.length(); i < size; i++) {
            JSONObject objectInArray = jsonArray.getJSONObject(i);
            ForgeResponse forgeResponse = new ForgeResponse();
            forgeResponse.setStatus(objectInArray.getString("status"));
            forgeResponse.setDescription(objectInArray.getString("description"));
            forgeResponse.setNumber(objectInArray.getString("number"));
            forgeResponse.setUserType(objectInArray.getString("user_type"));
            forgeResponse.setRequestType(objectInArray.getString("request_type"));
            forgeResponse.setRequestNumber(objectInArray.getString("request_number"));
            forgeResponse.setClientId(objectInArray.getString("client_id"));
            forgeResponse.setCreatedDate(objectInArray.getString("created_date"));
            forgeResponse.setNotifyDate(objectInArray.getString("notify_date"));
            forgeResponse.setUserO2Id(objectInArray.getString("user_o2_id"));
            forgeResponse.setUserName(objectInArray.getString("user_name"));
            forgeResponse.setUserEmail(objectInArray.getString("user_email"));
            forgeResponse.setAppName(objectInArray.getString("app_name"));
            forgeResponses.add(forgeResponse);
        }
        return forgeResponses;
    }

    public void getScheduledUserUpdate(String clientId) throws JsonProcessingException, EntityNotFoundException, InterruptedException {
        log.info("<====== started getScheduledUserUpdate() =======> ");
//        List<String> taskIdList = this.excelDataTaskRepository.findAllByTaskIdAndStatus();
//        if (taskIdList.isEmpty()){
//            log.info("No data found");
//        }else {
        log.info("<============================================================================================>");
        log.info("<===============TOTAL THREAD COUNT BEFORE THE FUNCTION ==============>" + Thread.activeCount());
        log.info("<============================================================================================>");
        List<LambdaResponse> lambdaResponseList = this.lambdaResponseRepository.getAllLambdaByTime(clientId);
        if (lambdaResponseList.isEmpty()) {
            log.error("No data found for the forge update" + lambdaResponseList);
        } else {
            List<TaskIdListRequest> taskIdListRequests = new ArrayList<>();
            List<String> taskIds = new ArrayList<>();
            List<String> taskIdListss = new ArrayList<>();
            for (LambdaResponse lambdaResponse : lambdaResponseList) {
                log.info("====== lambdaResponse ====== :: " + lambdaResponse);
//                VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(lambdaResponse.getClientId());
                List<LambdaRequestDto> lambdaRequestDtoArrayList = new ArrayList<>();
                lambdaResponseList.forEach(lambdaResponse1 -> {
                    if (lambdaResponse.getTaskId().equalsIgnoreCase(lambdaResponse1.getTaskId())) {
                        LambdaRequestDto lambdaRequestDto = new LambdaRequestDto();
                        lambdaRequestDto.setRequestId(lambdaResponse1.getRequestId());
                        lambdaRequestDto.setInstanceId(lambdaResponse1.getInstanceId());
                        lambdaRequestDtoArrayList.add(lambdaRequestDto);
                    }
                });
                log.info("list to sending data to qual =======:: " + lambdaRequestDtoArrayList.toString());
                LambdaReturnStatus lambdaReturnStatus = this.vendorService.qualtricsLambdaResponse(lambdaRequestDtoArrayList);
                log.info("====== lambdaResponse ====== :: " + lambdaReturnStatus);
                if (lambdaReturnStatus.getCode() != null) {
                    if (lambdaReturnStatus.getCode().equalsIgnoreCase("complete") && taskIds.indexOf(lambdaResponse.getTaskId()) == -1) {
                        lambdaReturnStatus.setCode("success");
                        log.info("====== taskId for save reason :: ====== " + lambdaResponse.getTaskId());
                        taskIds.add(lambdaResponse.getTaskId());

                        TaskIdListRequest taskIdListRequest = new TaskIdListRequest();
                        taskIdListRequest.setTaskId(lambdaResponse.getTaskId());
                        taskIdListRequest.setLambdaReturnStatus(lambdaReturnStatus);
                        taskIdListss.add(lambdaResponse.getTaskId());
                        log.info("======== databases save operation taskId's =====:: " + taskIdListRequests);
                        taskIdListRequests.add(taskIdListRequest);
                        if (!taskIdListRequests.isEmpty()) {
                            log.info("========more the 10 records to save the data=======" + taskIdListss);
                            this.updateLambdaResponseDatabases(taskIdListRequests, taskIdListss);
                        }
                        log.info("<=====================================================================>");
                        log.info("<===  TOTAL NUMBER OF THERAD BEFORE SLEEP  ===> " + Thread.activeCount());
                        log.info("<=====================================================================>");
                        Thread.sleep(750);
//                            ExcelDataTask excelDataTask1 = this.excelDataTaskRepository.findOneByNumber(lambdaResponse.getTaskId());
//                            excelDataTask1.setFetchStatus(true);
//                            log.info("saving data to excelDataTask :: "+excelDataTask1);
//                            this.excelDataTaskRepository.save(excelDataTask1);
//                        this.updateForgeApi(lambdaResponse, vendorDetails, lambdaReturnStatus);
                    }
                }
                log.info("<============================================================================================>");
                log.info("<=============== INSIDE THE LOOOP AND TOTAL THREAD ARE ==============>" + Thread.activeCount());
                log.info("<============================================================================================>");
            }
        }
        log.info("<============================================================================================>");
        log.info("<===============TOTAL THREAD COUNT AFTER THE FUNCTION ==============>" + Thread.activeCount());
        log.info("<============================================================================================>");
//        }
    }

    private void updateLambdaResponseDatabases(List<TaskIdListRequest> taskIdListRequest, List<String> taskIdList) {
        List<LambdaResponse> lambdaResponseList1 = this.lambdaResponseRepository.findAllByTaskId(taskIdList);
        List<LambdaResponse> lambdaResponse2 = new ArrayList<>();
        for (LambdaResponse lambdaResponse1 : lambdaResponseList1) {
            for (TaskIdListRequest taskIdListRequest1 : taskIdListRequest) {
                if (taskIdListRequest1.getTaskId().equalsIgnoreCase(lambdaResponse1.getTaskId())) {
                    lambdaResponse1.setUpdatedDate(new Date());
                    lambdaResponse1.setStatus(true);
                    lambdaResponse1.setNumberOfAttempt(1l);
                    lambdaResponse1.setResason(taskIdListRequest1.getLambdaReturnStatus().getReason());
                    lambdaResponse1.setForgeUpdateStatus(false);
                    lambdaResponse2.add(lambdaResponse1);
                }
            }
        }
        log.info("====== saved user data to the databases ====== :: " + lambdaResponse2);
        log.info("<=========================================================>");
        log.info("<=== TOTAL NUMBER OF THREAD CREATED  ===>" + Thread.activeCount());
        log.info("<===========================================================>");
        this.lambdaResponseRepository.saveAll(lambdaResponse2);
        taskIdListRequest.clear();
        taskIdList.clear();
        log.info("====== deleted user data for the taskId ====== :: " + taskIdList);
    }

    public void updateToForgeRequest(String clientId) {
        List<Object[]> lambdaResponseList = this.lambdaResponseRepository.findAllByFetchStatus(clientId);

        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientId);
        if (lambdaResponseList.isEmpty()) {
            log.info("======== no data found =========");
        } else {
            List<String> taskIds = new ArrayList<>();
            List<String> errorTaskId = new ArrayList<>();
            for (Object[] lambdaResponse : lambdaResponseList) {
                LambdaReturnStatus lambdaReturnStatus = new LambdaReturnStatus();
                lambdaReturnStatus.setCode("success");
                lambdaReturnStatus.setReason(lambdaResponse[1].toString());
                lambdaReturnStatus.setType(null);

                log.info("forge status object " + lambdaReturnStatus.toString());

                LambdaReturnValueRequest lambdaReturnValueRequest = new LambdaReturnValueRequest();
                lambdaReturnValueRequest.setTaskId(lambdaResponse[0].toString());
                lambdaReturnValueRequest.setStatus(lambdaReturnStatus);
                log.info("<====== lambdaReturnValueRequest values for the TaskId :: ======>" + lambdaReturnValueRequest.getTaskId());
                RestTemplate restTemplate = new RestTemplate();
                HttpHeaders headers = new HttpHeaders();
                headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
                headers.add(ResponseConstants.CLIENT_ID, lambdaResponse[2].toString());
                headers.add(ResponseConstants.CLIENT_SECRET, vendorDetails.getClientSecret());
                headers.add(ResponseConstants.GRANT_TYPE, grantType);
                ResponseEntity<String> res = restTemplate.postForEntity(url, headers, String.class);

                JSONObject jsonObject = new JSONObject(res.getBody());
                headers.setContentType(MediaType.APPLICATION_JSON);
                headers.set(ResponseConstants.AUTHORIZATION, jsonObject.getString(ResponseConstants.TOKEN_TYPE) + " " + jsonObject.getString(ResponseConstants.ACCESS_TOKEN));
                HttpEntity<LambdaReturnValueRequest> request = new HttpEntity<>(lambdaReturnValueRequest, headers);
                try {
                    ResponseEntity<String> resp = restTemplate.exchange(forgeUpdateUrl, HttpMethod.POST, request, String.class);
                    log.info("========= response for the taskId ==========" + resp.getStatusCodeValue());
                    if (resp.getStatusCodeValue() == 200) {
                        taskIds.add(lambdaResponse[0].toString());
                        if (taskIds.size() > 20) {
                            log.info("================================================================");
                            log.info("list of taskId for the delete" + taskIds);
                            log.info("================================================================");
                            this.updateForgeDetailToDB(taskIds);
                            taskIds.clear();
                            log.info("====== forge_update_status completed ====== :: ");
                        }
                    }
                } catch (Exception e) {
                    errorTaskId.add(lambdaResponse[0].toString());
                    if (errorTaskId.size() > 20) {
                        this.updateForgeDetailToDB(errorTaskId);
                        errorTaskId.clear();
                    }
                    log.info("====== forge_update_status completed for error taskId ====== :: ");
                    log.info("========= error for the taskId ==========" + lambdaResponse[0].toString());
                }
                log.info("<====== Ended updateForgeApi(LambdaResponse lambdaResponse, VendorDetails vendorDetails, LambdaReturnStatus lambdaReturnStatus) ======>");
            }
            if(taskIds.size()>0){
                log.info("<=== Started Final Update for forge  ===>",taskIds);
                this.updateForgeDetailToDB(taskIds);
                taskIds.clear();
                log.info("<=== Completed Final Update for forge  ===>",taskIds);
            }
        }
    }

    private void updateForgeDetailToDB(List<String> taskIds) {
        List<LambdaResponse> lambdaResponseList1 = this.lambdaResponseRepository.findAllByTaskId(taskIds);
        List<LambdaResponse> lambdaResponse2 = new ArrayList<>();
        log.info("====== started saving the status to Databases ====== :: ");
        for (LambdaResponse lambdaResponse1 : lambdaResponseList1) {
            lambdaResponse1.setUpdatedDate(new Date());
            lambdaResponse1.setStatus(true);
            lambdaResponse1.setForgeUpdateStatus(true);
            lambdaResponse1.setNumberOfAttempt(1l);
            lambdaResponse2.add(lambdaResponse1);
        }
        this.lambdaResponseRepository.saveAll(lambdaResponse2);
    }

    private void updateForgeApi(LambdaResponse lambdaResponse, VendorDetails vendorDetails, LambdaReturnStatus lambdaReturnStatus) {
        log.info("<====== Started updateForgeApi(LambdaResponse lambdaResponse, VendorDetails vendorDetails, LambdaReturnStatus lambdaReturnStatus) ======>");
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.add(ResponseConstants.CLIENT_ID, vendorDetails.getClientId());
        headers.add(ResponseConstants.CLIENT_SECRET, vendorDetails.getClientSecret());
        headers.add(ResponseConstants.GRANT_TYPE, grantType);
        ResponseEntity<String> res = restTemplate.postForEntity(url, headers, String.class);

        log.info("setting lambda function for taskId" + lambdaResponse.getTaskId());
        LambdaReturnValueRequest lambdaReturnValueRequest = new LambdaReturnValueRequest();
        lambdaReturnValueRequest.setStatus(lambdaReturnStatus);
        lambdaReturnValueRequest.setTaskId(lambdaResponse.getTaskId());
        JSONObject jsonObject = new JSONObject(res.getBody());
        headers.setContentType(MediaType.APPLICATION_JSON);
        headers.set(ResponseConstants.AUTHORIZATION, jsonObject.getString(ResponseConstants.TOKEN_TYPE) + " " + jsonObject.getString(ResponseConstants.ACCESS_TOKEN));
        HttpEntity<LambdaReturnValueRequest> request = new HttpEntity<>(lambdaReturnValueRequest, headers);
        ResponseEntity<String> resp = restTemplate.exchange(forgeUpdateUrl, HttpMethod.POST, request, String.class);
        log.info("====== response from the forge ====== :: " + resp);
        if (resp.getStatusCodeValue() == 200) {
            List<LambdaResponse> lambdaResponseList = this.lambdaResponseRepository.findAllByTaskId(lambdaResponse.getTaskId());
            List<LambdaResponse> lambdaResponse2 = new ArrayList<>();
            for (LambdaResponse lambdaResponse1 : lambdaResponseList) {
                lambdaResponse1.setUpdatedDate(new Date());
                lambdaResponse1.setStatus(true);
                lambdaResponse1.setNumberOfAttempt(1l);
                lambdaResponse2.add(lambdaResponse1);
            }
            this.lambdaResponseRepository.saveAll(lambdaResponse2);
            log.info("====== deleted user data for the taskId ====== :: " + lambdaResponse.getTaskId());
        }
        log.info("<====== Ended updateForgeApi(LambdaResponse lambdaResponse, VendorDetails vendorDetails, LambdaReturnStatus lambdaReturnStatus) ======>");
    }


    public Map deleteDataUsingExcel(String clientId) {
        log.info("<=== Started deleting user_details from lambda ===>");
        List<ExcelDataTask> excelData = excelDataTaskRepository.findByClientIdAndFetchStatus(clientId, false);

        if (excelData.size() < 1) {
            log.error("<=== No data present for the client id {} ===>", clientId);
            return ResponseJsonUtil.getCustomResponseJson("fail", "No data present");
        }
        List<String> lambdaResponses = lambdaResponseRepository.findByClientId(clientId);

        excelData.forEach(excelDataTask -> {
            if (lambdaResponses.indexOf(excelDataTask.getNumber()) == -1) {
                log.info("Started processing delete request for the task id {}", excelDataTask.getNumber());
                LambdaTestDto lambdaTestDto = new LambdaTestDto();
                lambdaTestDto.setEmails(Arrays.asList(excelDataTask.getEmailId()));
                lambdaTestDto.setFunctionName("gdpr_lithium_delete");
                try {
                    String payload = vendorService.qualtricsLambdaCall(lambdaTestDto);
                    log.info(payload);
                    boolean status = true;
                    JSONObject jsonObject = new JSONObject(payload);
                    if (jsonObject.getString("status").equalsIgnoreCase("success")) {
                        status = false;
                    }
                    LambdaResponse lambdaResponse = new LambdaResponse();
                    lambdaResponse.setStatus(status);
                    lambdaResponse.setUpdatedDate(new Date());
                    lambdaResponse.setClientId(excelDataTask.getClientId());
                    lambdaResponse.setForgeUpdateStatus(false);
                    lambdaResponse.setRequestId(excelDataTask.getRequestId());
                    lambdaResponse.setTaskId(excelDataTask.getNumber());
                    lambdaResponse.setResason(jsonObject.getString("data"));
                    lambdaResponse.setCreatedDate(new Date());
                    lambdaResponse.setInstanceId("PROD");
                    lambdaResponse.setUserId("");
                    lambdaResponse.setNumberOfAttempt((long) 1);
                    lambdaResponseRepository.save(lambdaResponse);
                    log.info("successfully Inserted data");
                } catch (JsonProcessingException e) {
                    e.printStackTrace();
                }
            }
        });
        log.info("<=== Completed deleting user_details from lambda ===>");
        return ResponseJsonUtil.getCustomResponseJson("success", "Successfully processed all the request");
    }

    public void deleteDataFromVendor(CommonLambdaRequestDTO commonLambdaRequestDTO) throws JsonProcessingException {
        log.info("<=== Started Lambda Function for the emailId {} functionName {}===>", commonLambdaRequestDTO.getEmailId() ,commonLambdaRequestDTO.getFunctionName());
        String payload="";
        if(commonLambdaRequestDTO.getFunctionName().equalsIgnoreCase("gdpr_twilio_delete")){
            log.info("<=== Started Lambda Function for delete twilio data for oxygenId "+commonLambdaRequestDTO.getUserId());
            payload = vendorService.commonLambdaCall(commonLambdaRequestDTO.getUserId(), commonLambdaRequestDTO.getFunctionName());
        }
        else{
            payload = vendorService.commonLambdaCall(commonLambdaRequestDTO.getEmailId(), commonLambdaRequestDTO.getFunctionName());
        }
        log.info(payload);
        boolean status = true;
        JSONObject jsonObject = new JSONObject(payload);
        LambdaResponse lambdaResponse = new LambdaResponse();
        if (commonLambdaRequestDTO.getFunctionName().contains("Directly")) {

            lambdaResponse.setStatus(false);
            lambdaResponse.setUpdatedDate(new Date());
            lambdaResponse.setClientId(commonLambdaRequestDTO.getClientId());
            lambdaResponse.setForgeUpdateStatus(false);
            lambdaResponse.setRequestId(commonLambdaRequestDTO.getRequestId());
            lambdaResponse.setTaskId(commonLambdaRequestDTO.getTaskId());
            if(jsonObject.has("status")){
                lambdaResponse.setResason(jsonObject.getJSONObject("result").getJSONObject("error").getString("message"));
            }else {
                lambdaResponse.setResason(jsonObject.getJSONObject("error").getString("message"));
            }
            lambdaResponse.setCreatedDate(new Date());
            lambdaResponse.setInstanceId(commonLambdaRequestDTO.getInstanceId());
            lambdaResponse.setUserId(commonLambdaRequestDTO.getUserId());
            lambdaResponse.setNumberOfAttempt((long) 1);


        }
        else if (commonLambdaRequestDTO.getFunctionName().equalsIgnoreCase("gdpr_vision-Critical_delete"))
        {

            log.info("Started forge update for Vision-critical");
            lambdaResponse.setStatus(true);
            lambdaResponse.setUpdatedDate(new Date());
            lambdaResponse.setClientId(commonLambdaRequestDTO.getClientId());
            lambdaResponse.setForgeUpdateStatus(false);
            lambdaResponse.setRequestId(commonLambdaRequestDTO.getRequestId());
            lambdaResponse.setTaskId(commonLambdaRequestDTO.getTaskId());
            lambdaResponse.setResason(payload);
            lambdaResponse.setCreatedDate(new Date());
            lambdaResponse.setInstanceId(commonLambdaRequestDTO.getInstanceId());
            lambdaResponse.setUserId(commonLambdaRequestDTO.getUserId());
            lambdaResponse.setNumberOfAttempt((long) 1);

        }

        // temperory fix for twillio , need to make it generic.
        else if (commonLambdaRequestDTO.getFunctionName().equalsIgnoreCase("gdpr_twilio_delete"))
        {

            log.info("Started forge update for Twillio");
            lambdaResponse.setStatus(true);
            lambdaResponse.setUpdatedDate(new Date());
            lambdaResponse.setClientId(commonLambdaRequestDTO.getClientId());
            lambdaResponse.setForgeUpdateStatus(false);
            lambdaResponse.setRequestId(commonLambdaRequestDTO.getRequestId());
            lambdaResponse.setTaskId(commonLambdaRequestDTO.getTaskId());
            lambdaResponse.setResason(jsonObject.getString("data"));
            lambdaResponse.setCreatedDate(new Date());
            lambdaResponse.setInstanceId(commonLambdaRequestDTO.getInstanceId());
            lambdaResponse.setUserId(commonLambdaRequestDTO.getUserId());
            lambdaResponse.setNumberOfAttempt((long) 1);

        }





        else {
            log.info("Started forge update ");
                if (!jsonObject.getString("status").equalsIgnoreCase("success")) {
                    status = false;
                }
                //  LambdaResponse lambdaResponse = new LambdaResponse();
                lambdaResponse.setStatus(status);
                lambdaResponse.setUpdatedDate(new Date());
                lambdaResponse.setClientId(commonLambdaRequestDTO.getClientId());
                lambdaResponse.setForgeUpdateStatus(status);
                lambdaResponse.setRequestId(commonLambdaRequestDTO.getRequestId());
                lambdaResponse.setTaskId(commonLambdaRequestDTO.getTaskId());
                lambdaResponse.setResason(jsonObject.getString("data"));
                lambdaResponse.setCreatedDate(new Date());
                lambdaResponse.setInstanceId(commonLambdaRequestDTO.getInstanceId());
                lambdaResponse.setUserId(commonLambdaRequestDTO.getUserId());
                lambdaResponse.setNumberOfAttempt((long) 1);
            }

            lambdaResponseRepository.save(lambdaResponse);
            log.info("<=== Completed Lambda Function for delete email {} ,functionName {}===>", commonLambdaRequestDTO.getEmailId(), commonLambdaRequestDTO.getFunctionName());

    }



    public void getAllTasksFromForge(ClientIdRequest clientIdRequest) throws JSONException, JsonProcessingException,
            EntityNotFoundException, InterruptedException {
        log.info("<======= Started getBackLogForge() service ========>");
        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientIdRequest.getClientId());
        if (vendorDetails == null) {
            log.error("No vendorDetails found for the clientId" + clientIdRequest.getClientId());
            throw new EntityNotFoundException(VendorDetails.class, "VendorDetails " + Constant.NOT_FOUND,
                    " clientId : " + clientIdRequest.getClientId());
        }
        ResponseEntity<String> response = this.getStringResponseEntity(vendorDetails);


        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject json = new JSONObject();
        jsonObject.getJSONArray(ResponseConstants.TASKS);
        JSONArray jsonArray = convertToJsonArray(response);
        log.info("Forge get tasks ==>> " + jsonObject.toString());

        CommonLambdaRequestDTO commonLambdaRequestDTO = new CommonLambdaRequestDTO();
        List<String> tasksId = this.lambdaResponseRepository.findByClientId(clientIdRequest.getClientId());
        log.info("===== task id ===== :: " + tasksId);

        commonLambdaRequestDTO.setClientId(clientIdRequest.getClientId());
        commonLambdaRequestDTO.setFunctionName(vendorDetails.getAssociateLambda());
        for (int i = 0, size = jsonArray.length(); i < size; i++) {
            JSONObject objectInArray = jsonArray.getJSONObject(i);
            if (tasksId.indexOf(objectInArray.getString("number")) == -1) {

                commonLambdaRequestDTO.setEmailId(objectInArray.getString("user_email"));
                commonLambdaRequestDTO.setRequestId(objectInArray.getString("request_number"));
                commonLambdaRequestDTO.setTaskId(objectInArray.getString("number"));
                commonLambdaRequestDTO.setUserId(objectInArray.getString("user_o2_id"));
                commonLambdaRequestDTO.setInstanceId("PROD");// hard coded on temporary basis
                deleteDataFromVendor(commonLambdaRequestDTO);
                log.info("<======= Started getBackLogForge() service ========>" + commonLambdaRequestDTO.toString());
            }


        }
    }

    public void getAllTasksFromForgeAndProcessForDelete(ClientIdRequest clientIdRequest) throws JSONException, JsonProcessingException,
            EntityNotFoundException, InterruptedException {
        log.info("<======= Started getBackLogForge() service ========>");
        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientIdRequest.getClientId());
        if (vendorDetails == null) {
            log.error("No vendorDetails found for the clientId" + clientIdRequest.getClientId());
            throw new EntityNotFoundException(VendorDetails.class, "VendorDetails " + Constant.NOT_FOUND,
                    " clientId : " + clientIdRequest.getClientId());
        }
        ResponseEntity<String> response = this.getStringResponseEntity(vendorDetails);


        JSONObject jsonObject = new JSONObject(response.getBody());
        JSONObject json = new JSONObject();
        jsonObject.getJSONArray(ResponseConstants.TASKS);
        JSONArray jsonArray = convertToJsonArray(response);
        log.info("Forge get tasks ==>> " + jsonObject.toString());

        CommonLambdaRequestDTO commonLambdaRequestDTO = new CommonLambdaRequestDTO();
        List<String> tasksId = this.lambdaResponseRepository.findByClientId(clientIdRequest.getClientId());
        log.info("===== task id ===== :: " + tasksId);

        commonLambdaRequestDTO.setClientId(clientIdRequest.getClientId());
        commonLambdaRequestDTO.setFunctionName(vendorDetails.getAssociateLambda());
        for (int i = 0, size = jsonArray.length(); i < size; i++) {
            JSONObject objectInArray = jsonArray.getJSONObject(i);
            log.info("<========OXYGEN IDS {} >",objectInArray.getString("user_o2_id"));
            if (tasksId.indexOf(objectInArray.getString("number")) == -1) {

                commonLambdaRequestDTO.setEmailId(objectInArray.getString("user_email"));
                commonLambdaRequestDTO.setRequestId(objectInArray.getString("request_number"));
                commonLambdaRequestDTO.setTaskId(objectInArray.getString("number"));
                commonLambdaRequestDTO.setUserId(objectInArray.getString("user_o2_id"));
                commonLambdaRequestDTO.setInstanceId("PROD");// hard coded on temporary basis
                deleteDataFromVendors(commonLambdaRequestDTO);
                log.info("<======= Started getBackLogForge() service ========>" + commonLambdaRequestDTO.toString());
            }
        }
    }


    //DELETE THE DATA FROM VENDORS WHO DIDNT GIVE UPDATE STATUS IMMEDIATELY
    public void deleteDataFromVendors(CommonLambdaRequestDTO commonLambdaRequestDTO) throws JsonProcessingException {
        log.info("<=== Started Deleting data from vendors who used to wait ===>");
        String payload="";
        if(commonLambdaRequestDTO.getFunctionName().equalsIgnoreCase("gdpr_twilio_delete") || commonLambdaRequestDTO.getFunctionName().equalsIgnoreCase("gdpr_AdobeAnalytics_delete")  ){
            log.info("<=== Started Lambda Function for delete twilio data for oxygenId "+commonLambdaRequestDTO.getUserId());
            payload = vendorService.commonLambdaCall(commonLambdaRequestDTO.getUserId(), commonLambdaRequestDTO.getFunctionName());
        }
        else{
            payload = vendorService.commonLambdaCall(commonLambdaRequestDTO.getEmailId(), commonLambdaRequestDTO.getFunctionName());
        }
        log.info(payload);
        JSONObject jsonObject = new JSONObject(payload);
        List<LambdaResponse> lambdaResponseList=new ArrayList<>();
        if(jsonObject.getJSONArray("data").length()>0){
            for(int i =0 ,size = jsonObject.getJSONArray("data").length(); i < size; i++) {
                LambdaResponse lambdaResponse = new LambdaResponse();
                lambdaResponse.setStatus(false);
                lambdaResponse.setUpdatedDate(new Date());
                lambdaResponse.setClientId(commonLambdaRequestDTO.getClientId());
                lambdaResponse.setForgeUpdateStatus(false);
                lambdaResponse.setRequestId(jsonObject.getJSONArray("data").getString(i));
                lambdaResponse.setTaskId(commonLambdaRequestDTO.getTaskId());
                lambdaResponse.setResason("");
                lambdaResponse.setCreatedDate(new Date());
                lambdaResponse.setInstanceId(commonLambdaRequestDTO.getInstanceId());
                lambdaResponse.setUserId(commonLambdaRequestDTO.getUserId());
                lambdaResponse.setNumberOfAttempt((long) 1);
                lambdaResponseList.add(lambdaResponse);
            }
            lambdaResponseRepository.saveAll(lambdaResponseList);
        }
        log.info("<=== Completed Deleting data from vendors who used to wait ===>");
    }


    public void checkUpdateDetailsOfVendor(ClientIdRequest clientIdRequest) throws JsonProcessingException, EntityNotFoundException {
        log.info("<=== Started checking the update details of vendor ===>");
        VendorDetails vendorDetails = this.vendorDetailsRepository.findOneByClientId(clientIdRequest.getClientId());
        List<LambdaResponse> lambdaResponseList = lambdaResponseRepository.findAllRequestIdByClientId(clientIdRequest.getClientId());
        String payload="";
        if(lambdaResponseList.size()<1){
            log.error("No vendorDetails found for the clientId" + clientIdRequest.getClientId());
            throw new EntityNotFoundException(LambdaResponse.class, "Request Ids " + Constant.NOT_FOUND,
                    " clientId : " + clientIdRequest.getClientId());
        }
        for (LambdaResponse lambdaResponse:lambdaResponseList) {
            payload = vendorService.commonLambdaUpdateCall(lambdaResponse.getRequestId(),vendorDetails.getAssociateLambda());
            JSONObject jsonObject = new JSONObject(payload);
            if(jsonObject.getString("status").equalsIgnoreCase("success")){
                lambdaResponse.setResason(jsonObject.getJSONObject("data").toString());
                lambdaResponse.setStatus(true);
                lambdaResponse.setUpdatedDate(new Date());
                this.lambdaResponseRepository.save(lambdaResponse);
            }
        }
        log.info("<=== Completed checking the update details of vendor ===>");
    }

}
