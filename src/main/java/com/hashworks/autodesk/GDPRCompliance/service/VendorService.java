package com.hashworks.autodesk.GDPRCompliance.service;

import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hashworks.autodesk.GDPRCompliance.dto.request.LambdaRequestDetailsDto;
import com.hashworks.autodesk.GDPRCompliance.dto.request.LambdaRequestDto;
import com.hashworks.autodesk.GDPRCompliance.dto.request.LambdaTestDto;
import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaResponseDto;
import com.hashworks.autodesk.GDPRCompliance.dto.response.LambdaReturnStatus;
import com.hashworks.autodesk.GDPRCompliance.repository.VendorDetailsRepository;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Slf4j
public class VendorService {

    @Autowired
    private VendorDetailsRepository vendorDetailsRepository;

    public String qualtricsLambdaCall(LambdaTestDto lambdaTestDto) throws JsonProcessingException {
        log.info("<========== Inside Qualtrics Lambda function qualtricsLambdaCall(LambdaTestDto lambdaTestDto)=============>");
        Map map1=new HashMap();
        String payload = "";
        map1.put("emails",lambdaTestDto.getEmails());
        String json = new ObjectMapper().writeValueAsString(map1);
        InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(lambdaTestDto.getFunctionName()).withPayload(ByteBuffer.wrap(json.getBytes()));
        InvokeResult invokeResult = awsLambdaClient().invoke(invokeRequest);
        payload = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
        log.info("<========= payload data =======>"+payload);
        return payload;
    }

    public String commonLambdaCall(String id,String functionName) throws JsonProcessingException {
        log.info("<========== Started Common Lambda function commonLambdaCall(String emailId ,String functionName) for email {} and functionName {}=============>",id,functionName);
        Map map1=new HashMap();
        String payload = "";
        if(functionName.equalsIgnoreCase("gdpr_twilio_delete") || functionName.equalsIgnoreCase("gdpr_AdobeAnalytics_delete")) {
            map1.put("oxygenId",id);
        }
        else{
            map1.put("emails", id);
        }
        String json = new ObjectMapper().writeValueAsString(map1);
        InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(functionName).withPayload(ByteBuffer.wrap(json.getBytes()));
        InvokeResult invokeResult = awsLambdaClient().invoke(invokeRequest);
        payload = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
        log.info("<========= payload data =======>"+payload);
        log.info("<========== Completed Common Lambda function commonLambdaCall(String emailId ,String functionName)=============>");
        return payload;
    }


    public String commonLambdaUpdateCall(String id,String functionName) throws JsonProcessingException {
        log.info("<========== Started Common Update Lambda function commonLambdaUpdateCall(String emailId ,String functionName) for email {} and functionName {}=============>",id,functionName);
        Map map1=new HashMap();
        String payload = "";
        map1.put("requestId",id);
        String json = new ObjectMapper().writeValueAsString(map1);
        InvokeRequest invokeRequest = new InvokeRequest().withFunctionName(functionName).withPayload(ByteBuffer.wrap(json.getBytes()));
        InvokeResult invokeResult = awsLambdaClient().invoke(invokeRequest);
        payload = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
        log.info("<========= payload data =======>"+payload);
        log.info("<========== Completed Common Update Lambda function commonLambdaUpdateCall(String emailId ,String functionName)=============>");
        return payload;
    }


    public LambdaReturnStatus qualtricsLambdaResponse(List<LambdaRequestDto> lambdaRequestDto) throws JsonProcessingException {
        log.info("<========== Inside Qualtrics Lambda function qualtricsLambdaResponse(LambdaRequestDto lambdaRequestDto) =============>");
        Map map1=new HashMap();
        List<LambdaRequestDetailsDto> lambdaRequestDetailsDtoArrayList = new ArrayList<>();
        String payload = "";
        for (LambdaRequestDto lambdaRequestDto1 : lambdaRequestDto){
            LambdaRequestDetailsDto lambdaRequestDetailsDto = new LambdaRequestDetailsDto();
            lambdaRequestDetailsDto.setRequestId(lambdaRequestDto1.getRequestId());
            lambdaRequestDetailsDto.setInstanceId(lambdaRequestDto1.getInstanceId());
            lambdaRequestDetailsDtoArrayList.add(lambdaRequestDetailsDto);
        }
        map1.put("data", lambdaRequestDetailsDtoArrayList);
        log.info("========= lambda request going to qualtrics ======= :: "+lambdaRequestDetailsDtoArrayList.toString());
        String json = new ObjectMapper().writeValueAsString(map1);
        String results="";
        InvokeRequest invokeRequest = new InvokeRequest().withFunctionName("gdpr_qualtrics_delete").withPayload(ByteBuffer.wrap(json.getBytes()));
        InvokeResult invokeResult = awsLambdaClient().invoke(invokeRequest);
        payload = new String(invokeResult.getPayload().array(), Charset.forName("UTF-8"));
        LambdaReturnStatus lambdaReturnStatus = new LambdaReturnStatus();
        log.info("<========= payload data =======>"+payload);
        JSONObject jsonObject = new JSONObject(payload);
        if (jsonObject.has("result")){
            JSONArray jsonArrayResults=jsonObject.getJSONArray("result");
            for (int j = 0, size2 = jsonArrayResults.length(); j < size2; j++) {
                results=results+jsonArrayResults.getJSONObject(j);
            }
            lambdaReturnStatus.setReason(results);
            lambdaReturnStatus.setType(null);
            lambdaReturnStatus.setCode(jsonObject.getString("status"));
        }else {
            log.info("No key word result in the response from the lambda"+jsonObject);
        }
        log.info("<====== End of the qualtricsLambdaResponse(LambdaRequestDto lambdaRequestDto) lambda response  ======> ::", lambdaReturnStatus);
        return lambdaReturnStatus;
    }

    @Bean
    public AWSLambda awsLambdaClient(){
        InstanceProfileCredentialsProvider provider = new InstanceProfileCredentialsProvider(true);
        return AWSLambdaClientBuilder.standard().withCredentials(provider).withRegion("us-west-2").build();
    }
}
