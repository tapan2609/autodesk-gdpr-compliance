package com.hashworks.autodesk.GDPRCompliance.utils;

import java.io.Serializable;

public final class ResponseConstants implements Serializable {

    protected static final String STATUS = "status";
    protected static final String FAILURE = "failure";
    protected static final String SUCCESS = "success";
    protected static final String DATA = "success";
    protected static final String ERROR = "success";
    public static final String TASKS = "tasks";
    public static final String RESULTS = "results";
    public static final String USEREMAIL = "user_email";
    public static final String NUMBER = "number";
    public static final String CLIENT_ID = "client_id";
    public static final String CLIENT_SECRET = "client_secret";
    public static final String GRANT_TYPE = "grant_type";
    public static final String AUTHORIZATION = "Authorization";
    public static final String TOKEN_TYPE = "token_type";
    public static final String ACCESS_TOKEN = "access_token";
    public static final String PARAMETERS = "parameters";
    public static final String USER = "user";
    public static final String REQUESTID ="requestId";
}
